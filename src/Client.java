import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.Scanner;

public class Client{
    public static void main(String[] args){
        try {
            Socket s = new Socket("localhost", 1234);
            InputStream is = s.getInputStream();
            ObjectInputStream ois = new ObjectInputStream(is);
            OutputStream os = s.getOutputStream();



            //Reception de l'objet liste de musique
            Object rep = ois.readObject();
            MusicList listeMusique = (MusicList)rep;

            Scanner scanner = new Scanner(System.in);
            System.out.println("Liste des musiques disponibles :");


            for (int i = 0; i < listeMusique.list.size(); i++) {
                System.out.println(i+" "+listeMusique.list.get(i).substring(0,listeMusique.list.get(i).length()-4));
            }


            System.out.print("Choisissez la musique : "+"\n");
            //Ecrire la suite pour un numéro inexistant
            int num = scanner.nextInt();
            //Choix de la musique envoyé.
            os.write(num);

            //On recoit un objet contenant les paroles et le midi
            Object rep2 = ois.readObject();
            MusicFile musique = (MusicFile)rep2;

            s.close();


            System.out.println("Choisissez les voix qui apparaitront : Homme (1), Femme (2), Choeur (3), Toutes (4)");
            int voix = scanner.nextInt();

            Midi monMidi = new Midi(musique.midifile);
            monMidi.Jouer();


            System.out.println("Lecture en cours : ");
            new Fenetre(num, voix);




        } catch (Exception e){

            e.printStackTrace();
        }

    }
}
