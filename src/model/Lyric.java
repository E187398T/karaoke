package model;

public class Lyric {

	private long timecode;
	private char voice;
	private String phrase;

	public Lyric(long timecode, char voice, String phrase) {
		this.timecode = timecode;
		this.voice = voice;
		this.phrase = phrase;
	}

	public long getTimecode() {
		return timecode;
	}

	public char getVoice() {
		return voice;
	}

	public String getPhrase() {
		return phrase;
	}
}
