package serveur;

import model.Request;
import service.SongService;

import java.io.Closeable;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import static config.RequestType.EXIT;

public class Server implements Closeable {

    private Socket socket;
    private ServerSocket serverSocket;
    private ObjectInputStream ois;
    private ObjectOutputStream oos;
    private SongService songService;

    Server() {
        songService = new SongService();
    }

    public void connect(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    public void accept() throws IOException {
        socket = serverSocket.accept();
        oos = new ObjectOutputStream(socket.getOutputStream());
        oos.flush();
        ois = new ObjectInputStream(socket.getInputStream());
    }

    @Override
    public void close() throws IOException {
        if (socket != null) {
            socket.close();
        }
        if (serverSocket != null) {
            serverSocket.close();
        }
    }

    public boolean handleClientRequest() throws IOException, ClassNotFoundException {
        Request request = (Request) ois.readObject();
        if (EXIT.equals(request.getType())) {
            return false;
        }
        oos.writeObject(processClientRequest(request));
        oos.flush();
        return true;
    }

    public Object processClientRequest(Request request) {
        switch (request.getType()) {
            case GET_LIST_OF_SONGS:
                return songService.getList();
            case GET_SONG_BY_INDEX:
                return songService.getByIndex((int) request.getArguments().get(0));
            default:
                return "Une erreur est survenue";
        }
    }
}