package service;

import model.Song;

import java.io.File;
import java.util.*;

public class SongService {

	private static final String LYRICS_PATH = "lyrics";
	private static final String MIDI_PATH = "midiFiles";

	public List<String> getList() {
		File lyricsRepository = new File(LYRICS_PATH);
		String[] files = lyricsRepository.list();

		if (files == null) {
			System.err.println("Nom de repertoire invalide");
			return new ArrayList<>();
		} else {
			List<String> songList = Arrays.asList(files);
			songList.sort(Comparator.naturalOrder());
			return songList;
		}
	}

	public Song getByIndex(int index) {
		File lyrics = new File(LYRICS_PATH + "/" + getList().get(index));
		File midi = new File(MIDI_PATH + "/" + getList().get(index).replace(".lrc", ".mid"));
		return new Song(midi, lyrics);
	}

}
