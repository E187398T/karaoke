package config;

public enum RequestType {
	GET_LIST_OF_SONGS,
	GET_SONG_BY_INDEX,
	EXIT
}
