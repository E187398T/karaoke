import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Server{

    public static void main(String[] args){
        try {
            ServerSocket ss = new ServerSocket(1234);
            System.out.println("\nJ'attends la connexion du client");
            Socket s = ss.accept();

            InputStream is = s.getInputStream();
            OutputStream os = s.getOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(os);


            //On récupère la liste des musiques disponible
            MusicList listeMusique = new MusicList();
            oos.writeObject(listeMusique);


            System.out.println("\nJ'attends le choix de musique du client");
            int num = is.read();


            //On cree un objet MusicFile pour l'envoyer au client
            System.out.println("\nJ'envoie la réponse");
            MusicFile musique = new MusicFile(num);
            oos.writeObject(musique);

            s.close();

        } catch (IOException e) {
            //Auto generate catch block
            e.printStackTrace();
        }
    }
}