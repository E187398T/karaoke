package client;

import config.RequestType;
import model.Request;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client implements Closeable {

	private Socket socket;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;

	public Client() {
	}

	public void connect(String host, int port) throws IOException {
		if (socket != null) {
			throw new IOException("Already connected");
		}
		socket = new Socket(host, port);
		OutputStream outputStream = socket.getOutputStream();
		oos = new ObjectOutputStream(outputStream);
		oos.flush();
		InputStream inputStream = socket.getInputStream();
		ois = new ObjectInputStream(inputStream);
	}

	@Override
	public void close() throws IOException {
		if (socket != null) {
			socket.close();
		}
	}


	public Object request(Request request) throws IOException, ClassNotFoundException {
		oos.writeObject(request);
		oos.flush();
		return ois.readObject();
	}


	public int readUserInput() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
	}

}
