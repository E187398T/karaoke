package client;

import config.Config;
import model.Request;
import model.Song;

import java.util.List;

import static config.RequestType.*;

public class Main {

	public static void printSongs(List<String> songs) {
		System.out.println("Liste des morceaux disponibles : ");
		for (int i=0 ; i<songs.size() ; i++) {
			System.out.println(i + " - " + songs.get(i));
		}
		System.out.println("Choisissez le numéro d'un morceau, ou entrez -1 pour quitter\n");
	}

	public static void main(String[] args) {
		try (Client client = new Client()) {
			// Connexion au serveur
			client.connect(Config.HOST_NAME, Config.PORT);
			System.out.println("Connexion établie avec le serveur");

			// Récupération et affichage de la liste des morceaux
			List<String> songNames = (List<String>) client.request(new Request(GET_LIST_OF_SONGS));
			printSongs(songNames);

			// Choix de l'utilisateur
			int index = client.readUserInput();
			if (index < 0) {
				System.out.println("Au revoir !");
				client.request(new Request(EXIT));
			} else {
				// Récupération de la chanson
				Song song = (Song) client.request(new Request(GET_SONG_BY_INDEX, index));

				// Lecture de la chanson
				Player player = new Player(song);
				player.openWindow();
				player.play();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}