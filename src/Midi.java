// Java program showing the implementation of a simple record

import javax.sound.midi.*;
import java.io.File;
import java.io.IOException;


public class Midi {
    File midi;


    public Midi(File midifile){
        this.midi = midifile;
    }


    public void Jouer() throws IOException, MidiUnavailableException, InvalidMidiDataException {

        //On cree une sequence
        Sequence sequence;

        //On associe notre fichier midi a notre sequence
        sequence = MidiSystem.getSequence(this.midi);

        //Recupere la duree de la sequence en microseconde
        //long time = sequence.getMicrosecondLength();
        //System.out.println(time);

        // On cree un sequencer pour la sequence
        Sequencer sequencer = MidiSystem.getSequencer();
        sequencer.open();

        //On associe notre sequence a notre sequencer
        sequencer.setSequence(sequence);

        //Pour modifier le tempo
        //sequencer.setTempoFactor(1f);  //Fonctionne

        // Start playing
        sequencer.start();

        //Stop playing
        //sequencer.stop();

    }
}



    //Bouts de codes utiles
    /*

    Track[] track = sequence.getTracks();
    sequencer.setTrackMute(track[0]);

    sequencer.setTempoInBPM(1250f);

    sequencer.setTempoFactor(1.5f);


     */



