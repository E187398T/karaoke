package serveur;

import config.Config;

public class Main {

	public static void main(String[] args) {
		try (Server server = new Server()) {
			// Création du serveur
			server.connect(Config.PORT);
			System.out.println("Le serveur vient de démarrer. En attente de client...");

			// Connexion du client
			server.accept();
			System.out.println("Un client vient de se connecter");

			// Lecture de la requête du client
			while (server.handleClientRequest()) {
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
