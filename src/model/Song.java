package model;

import java.io.File;
import java.io.Serializable;

public class Song implements Serializable {

	private File midi;
	private File lyrics;

	public Song(File midi, File lyrics) {
		this.midi = midi;
		this.lyrics = lyrics;
	}

	public File getMidi() {
		return midi;
	}

	public void setMidi(File midi) {
		this.midi = midi;
	}

	public File getLyrics() {
		return lyrics;
	}

	public void setLyrics(File lyrics) {
		this.lyrics = lyrics;
	}

}
