package model;

import config.RequestType;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class Request implements Serializable {
	private RequestType type;
	private List<Object> arguments;

	public Request(RequestType type, Object ...arguments) {
		this.type = type;
		this.arguments = Arrays.asList(arguments);
	}

	public RequestType getType() {
		return type;
	}

	public List<Object> getArguments() {
		return arguments;
	}
}
