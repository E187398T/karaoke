package client;

import mapper.LyricMapper;
import model.Lyric;
import model.Song;

import javax.sound.midi.*;
import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static java.awt.Color.*;

public class Player extends JFrame {

	Sequencer sequencer;
	Song song;
	List<Lyric> lyrics;

	Player(Song song) {
		this.song = song;
	}

	public void openWindow() {
		this.setTitle(song.getMidi().getName());
		this.setSize(300, 300);
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setVisible(true);

		JPanel pan = new JPanel();
		JLabel jlabel = new JLabel(song.getMidi().getName());
		jlabel.setForeground(BLACK);
		pan.add(jlabel);
		setContentPane(pan);
	}

	public void play() throws InvalidMidiDataException, IOException, MidiUnavailableException {
		sequencer = MidiSystem.getSequencer();
		sequencer.open();
		sequencer.setSequence(MidiSystem.getSequence(song.getMidi()));
		lyrics = createLyricsList();
		sequencer.start();
		playLyrics();
	}

	private List<Lyric> createLyricsList() throws IOException {
		String line;
		List<Lyric> lyrics = new ArrayList<>();
		BufferedReader br = new BufferedReader(new FileReader(song.getLyrics()));

		while ((line = br.readLine()) != null) {
			lyrics.add(LyricMapper.StringToLyric(line));
		}
		br.close();
		return lyrics;
	}

	private void playLyrics() {
		long startDate = System.currentTimeMillis();
		long trackDuration = sequencer.getSequence().getMicrosecondLength() / 1000;
		Long currentTrackTime;
		int index = 0;

		while ((currentTrackTime = System.currentTimeMillis() - startDate) < trackDuration && index < lyrics.size()) {
			Lyric nextLyric = lyrics.get(index + 1);
			if (nextLyric.getTimecode() < currentTrackTime) {
				index += 1;
				updateLyric(index);
			}
		}
	}

	private void updateLyric(int index) {
		JPanel pan = new JPanel();
		Lyric lyric = lyrics.get(index);
		JLabel jlabel = new JLabel(lyric.getPhrase());
		jlabel.setForeground(getColorFromVoice(lyric.getVoice()));
		pan.add(jlabel);
		setContentPane(pan);
		revalidate();
		repaint();
	}

	public void changeTempo(Float tempo) {
		sequencer.setTempoFactor(tempo);
	}

	public Color getColorFromVoice(char c) {
		switch (c) {
			case 'h':
				return BLUE;
			case 'f':
				return MAGENTA;
			case 'c':
				return GREEN;
			default:
				return BLACK;
		}
	}

}
