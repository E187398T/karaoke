package mapper;

import model.Lyric;

public class LyricMapper {

	public static Lyric StringToLyric(String str) {
		long min = Integer.valueOf(str.substring(1,3));
		long sec = Integer.valueOf(str.substring(4,6));
		long mil = Integer.valueOf(str.substring(7,9));
		long timecode = mil + 1000 * sec + 1000 * 60 * min;
		char voice = str.substring(10,11).charAt(0);
		String phrase = str.substring(12);
		return new Lyric(timecode, voice, phrase);
	}

}
